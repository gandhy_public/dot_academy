  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Product</h3>
            </div>
            <!-- /.box-header -->
            <form role="form" action="/product/{{ $dataProduct->id }}/update" method="post">
              @csrf
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <td>Id</td>
                  <td>:</td>
                  <td><input type="text" name="id" value="{{ $dataProduct->id }}" readonly="true"></td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><input type="text" name="nama" value="{{ $dataProduct->nama }}"></td>
                </tr>
                <tr>
                  <td>Kategori</td>
                  <td>:</td>
                  <td><input type="text" name="category" value="{{ $dataProduct->category }}"></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>:</td>
                  <td><input type="text" name="unit_price" value="{{ $dataProduct->unit_price }}"></td>
                </tr>
              </table>
              <br>
              <input type="submit" class="btn btn-primary" value="Update"> <a href="/product" class="btn btn-warning">Back</a>
            </form>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')