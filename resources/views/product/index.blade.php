  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/product/store" method="post">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Product Name</label>
                  <input type="text" name="nama" class="form-control" placeholder="Masukkan nama kategori produk">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Category</label>
                  <input type="text" name="category" class="form-control" placeholder="Masukkan nama kategori produk">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="text" name="unit_price" class="form-control" placeholder="Masukkan nama kategori produk">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Contoh Tabel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>Nama Product</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
                @foreach($dataProduct as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->unit_price }}</td>
                  <td>
                      <a href="/product/{{ $item->id }}">Detail</a> | <a href="/product/{{ $item->id }}/edit">Edit</a> | <a href="/product/{{ $item->id }}/delete">Delete</a>
                  </td>
                </tr>                  
                @endforeach
              </table>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')