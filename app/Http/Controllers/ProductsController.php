<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

class ProductsController extends Controller
{
    public function index()
	{
		$dataProduct = Products::all();

		return view('product.index', compact('dataProduct'));
	}

	public function detail($id)
	{
		$dataProduct = Products::find($id);

		return view('product.detail', compact('dataProduct'));
	}

	public function edit($id)
	{
		$dataProduct = Products::find($id);

		return view('product.edit', compact('dataProduct'));
	}

	public function store(Request $request)
	{
		$dataProduct = new Products;

		$dataProduct->nama = $request->nama;
		$dataProduct->category = $request->category;
		$dataProduct->unit_price = $request->unit_price;

		$dataProduct->save();
		
        return redirect()->back();
	}

	public function update(Request $request, $id)
	{
		$dataProduct = Products::find($id);

		$dataProduct->nama = $request->nama;
		$dataProduct->category = $request->category;
		$dataProduct->unit_price = $request->unit_price;

		$dataProduct->save();

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataProduct = Products::find($id);

		$dataProduct->delete();

		return redirect()->back();
	}
}
