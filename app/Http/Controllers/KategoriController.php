<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
	public function submit(Request $request)
	{
		$data = $request->all();
		return view('template',['kategori' => $data]);
	}
}