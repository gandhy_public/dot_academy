<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template',['kategori' => []]);
});

Route::get('/nama', function (){
	return "Gandhy";
});

Route::get('/helo/{nama}/{sapa}', function($jeneng,$sapa){
	return "Hello $jeneng $sapa";
});

Route::get('/sapa', 'CobaController@sapa');
Route::get('/sapa/{nama}', 'CobaController@sapaNama');

Route::post('/kategori', 'KategoriController@submit');


//route untuk product
Route::get('/product', 'ProductsController@index');
Route::get('/product/{id}', 'ProductsController@detail');
Route::get('/product/{id}/edit', 'ProductsController@edit');
Route::post('/product/store', 'ProductsController@store');
Route::post('/product/{id}/update', 'ProductsController@update');
Route::get('/product/{id}/delete', 'ProductsController@delete');


